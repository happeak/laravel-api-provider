<?php

return [
    'shop_id'  => env('HAPPEAK_SHOP_ID'),
    'shop_key' => env('HAPPEAK_SHOP_KEY'),
];