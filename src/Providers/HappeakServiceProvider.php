<?php

namespace Happeak\Providers;

use Happeak\ApiClient;
use Illuminate\Support\ServiceProvider;

class HappeakServiceProvider extends ServiceProvider
{

    protected $defer = false;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/happeak.php' => config_path('happeak.php'),
        ]);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ApiClient::class, function () {
            return new ApiClient(config('happeak.shop_id'), config('happeak.shop_key'));
        });
    }
}